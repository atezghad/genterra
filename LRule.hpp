#ifndef LRULE_H
#define LRULE_H

#include <string>
/**
* \file LRule.hpp
* \author Anis Tezghadanti
* */

/**
 * \class LRule
 * \brief La classe LRule permet de définir une règle de production
 * */
class LRule
{
	public:
		/**
		 * \brief Constructeur de l'objet LRule par défaut
		 * */
		LRule();
		/**
		 * \brief Constructeur de l'objet LRule
		 * \param nbIter Le nombre de fois que la règle sera reproduite
		 * \param symbol Le symbole de production
		 * \param rule La règle de production
		 * */
		LRule(const unsigned int nbIter, char symbol, std::string rule);

		/**
		 * \brief Retourne la règle reproduite nbIter fois
		 * \param nbIter Le nombre de fois que la règle sera reproduite
		 * \return La règle reproduite nbIter fois
		 * */
		std::string getRule(const unsigned int nbIter);

		/**
		 * \brief Retourne la règle de production actuelle
		 * \return La règle de production actuelle
		 * */
		std::string getCurrent() const;

	protected:
		char _symbol;
		std::string _rule;
		std::string _current;
		int _nbIter;
};

#endif // LRULE_H

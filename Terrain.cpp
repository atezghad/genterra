#include <cstdlib>
#include <iostream>
#include <cassert>
#include <cmath>
#include <fstream>
#include <algorithm>
#include <tuple>
#include <omp.h>

#include <Terrain.hpp>
#include <Utils/CImg.h>
#include <Utils/Random.hpp>

#define cimg_use_png
#define GRAD_TEMP 0.00065
#define AVG_TEMP 15.0
#define _GLIBCXX_PARALLEL

int Terrain::triangleCount = 0;

static Cle make_cle(int a, int b)
{
	if(std::min(a, b) == a)
		return Cle(a, b);
	return Cle(b, a);
}

void Terrain::init(std::initializer_list<Materiau> &list_m, std::initializer_list<Environnement::Espece> &list_species, const bool t)
{
	std::initializer_list<Materiau>::iterator it2 = list_m.begin();
	for(uint i = 0; i < list_m.size(); ++i)
	{
		m_layers.push_back(*it2);
		++it2;
	}

	std::sort(m_layers.begin(), m_layers.end(), compareVMass);

	std::initializer_list<Environnement::Espece>::iterator it3 = list_species.begin();
	for(uint i = 0; i < list_species.size(); ++i)
	{
		Environnement::Espece e = *it3;
		e.setId(i);
		m_species.push_back(e);
		++it3;
	}
	computePoints(t);
	computeFaces();
	computeNormalFaces();
	computeNormalVertices();
	computeNeighbors();
	computePlants();
}

Terrain::Terrain(const Coords2D &a, const Coords2D &b,
				 const uint &stepX, const uint &stepY,
				 std::initializer_list<Octave> list_octaves,
				 std::initializer_list<Materiau> list_m,
				 std::initializer_list<Environnement::Espece> list_species) :
	m_origin(a), m_arrival(b),
	m_step_x(stepX), m_step_y(stepY),
	m_x(std::abs(b.x()-a.x())), m_y(std::abs(b.y()-a.y()))
{
	std::cout << "Génération du terrain à partir de données..." << std::endl;
	assert(m_step_x > 0 && m_step_x < m_x && (m_x % m_step_x == 0));
	assert(m_step_y > 0 && m_step_y < m_y && (m_y % m_step_y == 0));


	std::initializer_list<Octave>::iterator it = list_octaves.begin();
	for(uint i = 0; i < list_octaves.size(); ++i)
	{
		m_octaves.push_back(*it);
		++it;
	}

	init(list_m, list_species, true);
}

Terrain::Terrain(const Coords2D &a,
				 const std::string& img, double minY, double maxY,
				 std::initializer_list<Materiau> list_m,
				 std::initializer_list<Environnement::Espece> list_species) :
	m_origin(a), m_step_x(1), m_step_y(1)
{
	std::cout << "Génération du terrain à partir de l'image " << img << " ..." << std::endl;
	cimg_library::CImg<double> src(img.c_str());
	m_x = src.width()-1;
	m_y = src.height()-1;

	if(minY > maxY)
		std::swap(minY, maxY);

	src.mirror('x');
	m_arrival = {(double)m_x + m_origin.x(), (double)m_y + m_origin.y()};

	for(uint y = 0; y <= m_y; ++y )
	{
		for(uint x = 0; x <= m_x; ++x )
		{
			double u = src(x,y) / 255.;
			m_bedRock.push_back((1. - u)*minY + u*maxY);
		}
	}
	init(list_m, list_species, false);
}

void Terrain::addOctave(const double &amp, const double &f, const double &t)
{
	Octave o(amp, f, t);
	m_octaves.push_back(o);
}

double Terrain::getY(const Coords2D &p)
{
	assert(p.x() >= m_origin.x() && p.x() <= m_arrival.x());
	assert(p.y() >= m_origin.y() && p.y() <= m_arrival.y());
	double z = 0;
	for(uint i = 0; i < m_octaves.size(); ++i)
	{
		z+= m_octaves[i].getValue(p.x(), p.y());
	}
	return z;
}

void Terrain::computeY()
{
	if(!m_bedRock.empty())
		m_bedRock.clear();
	for(uint j = 0; j <= m_y; j+=m_step_y)
	{
		for(uint i = 0; i <= m_x; i+=m_step_x)
		{
			m_bedRock.push_back(getY(Coords2D(m_origin.x() + i, m_origin.y() + j)));
		}
	}
}

void Terrain::computePoints(const bool t)
{
	std::cout << "Création des sommets..." << std::endl;
	int c = 0;
	if(t)
		computeY();

	for(uint z = 0; z <= m_y; z+=m_step_y)
	{
		for(uint x = 0; x <= m_x; x+=m_step_x)
		{
			Vertex p(m_origin.x()+x, m_bedRock.at(c), m_origin.y()+z, c);
			m_vertices.push_back(p);
			m_flow.push_back(.0);
			for(uint i = 0; i < m_layers.size(); ++i)
			{
				m_layers_vertices.push_back(m_layers.at(i));
			}
			++c;
		}
	}
	std::cout << "Création des sommets terminée!" << std::endl;
}



void Terrain::addAdjacency(Triangle &t, const int &p1, const int &p2, const int &i)
{
	std::map<Cle, Cle>::iterator it = m_adjacencies.find(make_cle(p1, p2));
	if(it != m_adjacencies.cend())
	{
		m_faces.at(it->second.second).ajouter_adjacent(i);
		t.ajouter_adjacent(it->second.second);
		it->second.first = i;
	}
	else m_adjacencies.insert(std::pair<Cle, Cle>(make_cle(p1, p2), make_cle(-1, i)));
}

void Terrain::addAdjacencies(Triangle &t, const int &i)
{
	addAdjacency(t, t.at(0), t.at(1), i);
	addAdjacency(t, t.at(0), t.at(2), i);
	addAdjacency(t, t.at(2), t.at(1), i);
}

void Terrain::setIncidents(Triangle &t, const int &i)
{
	for(uint j = 0; j < 3; ++j)
	{
		if(m_vertices[ t.at(j) ].incident() == -1)
		{
			m_vertices[ t.at(j) ].setIncident(i);
		}
	}
}

void Terrain::addFace(Triangle &t)
{
	addAdjacencies(t, triangleCount);
	setIncidents(t, triangleCount);
	t.setIndex(triangleCount);
	m_faces.push_back(t);
	++(triangleCount);
}

void Terrain::computeFaces()
{
	std::cout << "Création des faces..." << std::endl;
	int p_1 = 0;
	int p_2 = 0;
	int p_3 = 0;
	int p_4 = 0;
	for(uint z = 0; z < m_y/m_step_y; ++z)
	{
		for(uint x = 0; x < m_x/m_step_x; ++x)
		{
			p_1 = x + z*(m_x/m_step_x+1);
			p_2 = p_1 + m_x/m_step_x + 2 ;
			p_3 = p_2 - 1;
			p_4 = p_1 + 1;
			Triangle t1(p_1, p_3, p_2);
			Triangle t2(p_1, p_2, p_4);
			addFace(t1);
			addFace(t2);
		}
	}
	std::cout << "Création des faces terminée!" << std::endl;
	m_adjacencies.clear();
}

void Terrain::computeNormalFaces()
{
	for(std::vector<Triangle>::iterator it = m_faces.begin(); it != m_faces.end(); ++it)
	{
		Vertex p1 = m_vertices.at(it->at(0));
		Vertex p2 = m_vertices.at(it->at(1));
		Vertex p3 = m_vertices.at(it->at(2));
		Vec3D a(p1, p2);
		Vec3D b(p1, p3);

		Vec3D n = a.cross(b);
		n.normalize();
		m_normals.push_back(n);
	}
}

void Terrain::computeNormalVertices()
{
	for(std::vector<Vertex>::iterator it = m_vertices.begin(); it != m_vertices.end(); ++it)
	{
		FaceCirculator f = face_incident(*it);
		Triangle * t = &(*f);
		std::list<int> connexes;
		while(t != nullptr)
		{
			connexes.push_back(t->index());
			++f;
			t = &(*f);
		}
		Vec3D n;
		for(std::list<int>::iterator it_t = connexes.begin(); it_t != connexes.end(); ++it_t)
		{
			n+=m_normals.at(*it_t);
		}
		n.normalize();
		it->setNormal(n);
	}
}


void Terrain::computePlants()
{
	std::cout << "Placement des plantes en cours..." << std::endl;
	uint n = 0;

	for(std::vector<Environnement::Espece>::iterator species = m_species.begin(); species != m_species.end(); ++species)
	{
		n = 0;
		while(n != species->newPlants())
		{
			if(addPlant(species->id(), randomDouble(0, m_x), randomDouble(0, m_y)))
				++n;
		}
	}
	std::cout << "Placement des plantes terminé!" << std::endl;
}

void Terrain::computeNeighbors()
{
	for(std::vector<Vertex>::iterator it = m_vertices.begin(); it != m_vertices.end(); ++it)
	{
		VertexCirculator v = vertex_incident(*it);
		Vertex vnew = *v;
		Vertex vold;
		do
		{
			vold = vnew;
			it->addNeighbor(vnew.index());
			++v;
			vnew = *v;
		}
		while(vold != vnew);
	}

}

uint Terrain::at(const int i, const int j)
{
	return i + j*(m_x/m_step_x+1);
}

double Terrain::atI(const double x, const double y, const bool h)
{

	if((std::fmod(x, m_step_x) == 0. || std::fmod(x, m_step_x) == .1)
			&& ( std::fmod(y, m_step_y) == 0. || std::fmod(y, m_step_y) == .1))
	{
		int i = at(x/m_step_x,y/m_step_y);
		return (h)? m_bedRock[i] : m_flow[i];
	}
	else
	{
		int i = x/m_step_x;
		int j = y/m_step_y;

		std::vector<double> * val = (h)? &m_bedRock : &m_flow;
		int f1 = at(i, j);
		int f2 = at(i, j+1);
		int f3 = at(i+1, j);
		int f4 = at(i+1, j+1);

		double R1 = (((i+1)*m_step_x - x)/(m_step_x))*(*val)[f1] + ((x - i*m_step_x )/(m_step_x))*(*val)[f3];
		double R2 = (((i+1)*m_step_x - x)/(m_step_x))*(*val)[f2] + ((x - i*m_step_x )/(m_step_x))*(*val)[f4];
		return ((((j+1)*m_step_y - y)/(m_step_y))*R1 + ((y - j*m_step_y)/m_step_y)*R2);
	}
}

double Terrain::atILayer(const double x, const double y, const int m)
{

	if((std::fmod(x, m_step_x) == 0. || std::fmod(x, m_step_x) == .1)
			&& ( std::fmod(y, m_step_y) == 0. || std::fmod(y, m_step_y) == .1))
	{
		int i = at(x/m_step_x,y/m_step_y);
		return m_layers_vertices[i * m_layers.size() + m].height();
	}
	else
	{
		int i = x/m_step_x;
		int j = y/m_step_y;

		int f1 = at(i, j);
		int f2 = at(i, j+1);
		int f3 = at(i+1, j);
		int f4 = at(i+1, j+1);

		double R1 = (((i+1)*m_step_x - x)/(m_step_x))* m_layers_vertices[f1 * m_layers.size() + m].height()
					+ ((x - i*m_step_x )/(m_step_x))*m_layers_vertices[f3 * m_layers.size() + m].height();
		double R2 = (((i+1)*m_step_x - x)/(m_step_x))*m_layers_vertices[f2 * m_layers.size() + m].height()
					+ ((x - i*m_step_x )/(m_step_x))*m_layers_vertices[f4 * m_layers.size() + m].height();
		return ((((j+1)*m_step_y - y)/(m_step_y))*R1 + ((y - j*m_step_y)/m_step_y)*R2);
	}
}

int Terrain::whichRegion(const double &x, const double &y)
{
	if(x < m_x/2)
	{
		if(y < m_y/2)
		{
			return 0;
		}
		else return 3;
	}
	else
	{
		if(x >= m_x/2)
		{
			if(y >= m_y/2)
			{
				return 2;
			}
			else return 1;
		}
		return -1;
	}
}

bool Terrain::addPlant(const uint &id, const double &x, const double &y, const double o, const bool root)
{
	Coords2D q(x, y);
	int to_insert = whichRegion(x,y);
	if(to_insert != -1)
	{
		Environnement::Plante p(m_species.at(id), 1.0, q.x(), q.y(), o, root);
		bool conflict = false;
		for(auto plant = m_plants.at(to_insert).begin(); plant != m_plants.at(to_insert).end(); ++plant)
		{
			if(p.competition(*plant))
			{
				conflict = true;
				break;
			}
		}
		if(!conflict)
		{
			m_plants.at(to_insert).push_back(p);
		}
		return !conflict;
	}
	return false;
}


int Terrain::nextPoint(const int &i)
{
	std::list<int> neigh = m_vertices.at(i).neighbors();
	std::vector<int> possibilities;
	Vec3D to;
	for(std::list<int>::iterator n = neigh.begin(); n != neigh.end(); ++n)
	{
		to = Vec3D(m_vertices[i], m_vertices[*n]);
		if(to.y() < 0.0)
		{
			possibilities.push_back(*n);
		}
	}
	if(possibilities.empty())
	{
		return -1;
	}
	else return possibilities.at(randomInt(0, possibilities.size()-1));
}

void Terrain::simulateLayer(const int &m)
{
	double aR, newh, sum_alt;
	int v_i;
	Vertex v;
	Vec3D to;
	for(uint i = 0; i < m_vertices.size(); ++i)
	{
		aR = m_vertices.at(i).normal().angleDeg(Vec3D::Y);
		v_i = m_vertices.at(i).index() * m_layers.size() + m;
		newh = m_layers_vertices.at(v_i).computeHeight(aR);
		sum_alt = 0.;

		double toGive = 0.;
		if(newh != -1)
		{
			std::list<int> n = m_vertices.at( m_vertices.at(i).index() ).neighbors();
			std::list< std::pair<int, double> > next;
			for(std::list<int>::iterator itn = n.begin(); itn != n.end(); ++itn)
			{
				v = m_vertices.at(*itn);
				to = Vec3D(m_vertices.at(i), v);
				if(to.y() < 0.)
				{
					sum_alt+=std::abs(to.y());
					next.push_back(std::pair<int, double>(*itn, to.y()));
				}
			}
			if(!next.empty())
			{
				m_layers_vertices.at(v_i).decreaseHeight(newh);
				for(std::list< std::pair<int, double> >::iterator itg = next.begin(); itg != next.end(); ++itg)
				{
					toGive = newh * (std::abs(itg->second) / sum_alt);
					m_layers_vertices.at( (itg->first) * m_layers.size() + m).addHeight(toGive);
				}
			}
		}
	}
}


void Terrain::simulateFlow()
{
	int n = 0;
	double max = -1.0;

#pragma omp parallel for private(n) shared(max) schedule(dynamic)
	for(uint i = 0; i < m_vertices.size(); ++i)
	{
#pragma omp atomic
		m_flow[i]++;
		n = nextPoint(i) ;
		while(n != -1)
		{
#pragma omp atomic
			m_flow[n]++;
			if(m_flow[n] > max)
				max = m_flow[n];
			n = nextPoint(n);
		}
	}
	std::for_each(m_flow.begin(), m_flow.end(), [max](double &n){ n/=max ;});
}

void Terrain::simulateEcoSystem()
{
	std::list<offspring> list_to_spread;
	for(uint i = 0; i < 4; ++i)
	{
		for(std::list<Environnement::Plante>::iterator plant = m_plants.at(i).begin(); plant != m_plants.at(i).end(); ++plant)
		{
			if(!plant->root())
			{
				offspring nPlants = plant->spread();
				list_to_spread.push_back(nPlants);
				plant->setRoot(true);
			}
			plant->grow(atI(plant->disc().center().x(), plant->disc().center().y(), false),
						computeTemperature(atI(plant->disc().center().x(), plant->disc().center().y())));

			if(plant->vigor() <= Environnement::Plante::VIGOR_MIN)
			{
				plant = m_plants.at(i).erase(plant);
				--plant;
			}
			else
			{
				for(std::list<Environnement::Plante>::iterator o_plant = m_plants.at(i).begin(); o_plant != m_plants.at(i).end(); ++o_plant)
				{
					if(plant != o_plant)
					{
						if(plant->competition(*o_plant))
						{
							double c_p1 = plant->competitivity();
							double c_p2 = o_plant->competitivity();
							if(c_p1 > c_p2)
							{
								if(randomDouble(0,1) > c_p2)
								{
									o_plant = m_plants.at(i).erase(o_plant);
									--o_plant;
								}
								else
								{
									plant = m_plants.at(i).erase(plant);
									--plant;
									break;
								}
							}
							else
							{
								if(randomDouble(0,1) > c_p1)
								{
									plant = m_plants.at(i).erase(plant);
									--plant;
									break;
								}
								else
								{
									o_plant = m_plants.at(i).erase(o_plant);
									--o_plant;
								}
							}
						}
					}
				}
			}
		}
	}


	for(std::list<offspring>::iterator ltp = list_to_spread.begin(); ltp != list_to_spread.end(); ++ltp)
	{
		for(offspring::iterator vtp = ltp->begin(); vtp != ltp->end(); ++vtp)
		{
			if(std::get<0>(*vtp).x() <= m_x && std::get<0>(*vtp).x() >= 0 && std::get<0>(*vtp).y() <= m_y && std::get<0>(*vtp).y() >= 0)
			{
				addPlant(std::get<3>(*vtp), std::get<0>(*vtp).x(), std::get<0>(*vtp).y(), std::get<1>(*vtp), std::get<2>(*vtp));
			}
		}
	}

}

void Terrain::erodeHydro()
{
#pragma omp parallel for
	for(uint i = 0; i < m_vertices.size(); ++i)
	{
		Vec3D n = m_vertices.at(i).normal();
		n = n.scalar(-m_flow.at(i));
		m_vertices.at(i).setX(n.x()/5. + m_vertices.at(i).x());
		m_vertices.at(i).setY(n.y()/5. + m_vertices.at(i).y());
		m_vertices.at(i).setZ(n.z()/5. + m_vertices.at(i).z());
	}
}

void Terrain::simulation(const uint &s)
{
	std::cout << "Simulation en cours..." << std::endl;
	for(uint i = 0; i < 10; ++i)
	{
		simulateFlow();
	}
	for(uint i = 0; i < s; ++i)
	{
		for(uint i = 0; i < m_layers.size(); ++i)
		{
			simulateLayer(i);
		}
		simulateEcoSystem();
		std::cout << "Simulation [" << i+1 << "/" << s << "] terminée" << std::endl;
	}
}

void Terrain::exportAll(const std::string name)
{
	exportHeightMap(name);
	exportLayersOBJ(name);
	exportLayersMap(name);
	exportFlowMap(name);
	exportPlantsOBJ(name);
	exportPlantsMap(name);
}

void Terrain::exportOBJ(const std::string filename)
{
	std::ofstream obj;
	obj.open(filename.c_str(), std::ios::in | std::ios::trunc);
	std::cout << "Export du terrain .OBJ..." << std::endl;

	for(unsigned int i = 0; i < m_vertices.size(); ++i)
	{
		obj << "v " << m_vertices[i].x() << " " <<  m_vertices[i].y() << " " << m_vertices[i].z() << std::endl;
	}
	for(unsigned int i = 0; i < m_vertices.size(); ++i)
	{
		obj << "vn " << m_vertices[i].normal().x() << " " <<  m_vertices[i].normal().y() << " " << m_vertices[i].normal().z() << std::endl;
	}
	for(unsigned int i = 0; i < m_faces.size(); ++i)
	{
		obj << "f " << m_faces[i].at(0) + 1 << "//" << m_faces[i].at(0) + 1
			<< " " <<  m_faces[i].at(1) + 1 << "//" << m_faces[i].at(1) + 1
			<< " " <<  m_faces[i].at(2) + 1 << "//" << m_faces[i].at(2) + 1 << std::endl;
	}

	obj.close();
	std::cout << "Export terminé!" << std::endl;
}

void Terrain::exportPlantsOBJ(const std::string filename)
{
	std::cout << "Export des plantes .OBJ..." << std::endl;

	for(uint i = 0; i < m_species.size(); ++i)
	{
		std::ofstream obj;
		std::string final;
		final += std::to_string(i);
		final += "_";
		final += filename;
		obj.open(final.c_str(), std::ios::in | std::ios::trunc);
		std::cout << "Export du .OBJ..." << std::endl;
		for(uint j = 0; j < 4; ++j)
		{
			for(Environnement::Plante plant : m_plants.at(j))
			{
				if(plant.species()->id() == i)
				{
					obj << plant.disc().center().x() << " "
						<< atI(plant.disc().center().x(), plant.disc().center().y()) << " "
						<< plant.disc().center().y()
						<< std::endl;
				}
			}
		}
		obj.close();
	}
	std::cout << "Export terminé!" << std::endl;
}

void Terrain::exportLayersOBJ(const std::string filename)
{

	std::cout << "Export des couches .OBJ..." << std::endl;
	for(uint m = 0; m < m_layers.size(); ++m)
	{
		std::ofstream obj;
		std::string final = m_layers[m].name();
		final+="_";
		final+= filename;;
		obj.open(final.c_str(), std::ios::in | std::ios::trunc);
		for(unsigned int i = 0; i < m_vertices.size(); ++i)
		{
			double totalH = m_vertices[i].y();
			for(uint m_p = 0; m_p <= m; ++m_p)
			{
				totalH+= m_layers_vertices[i* m_layers.size() + m_p].height();
			}
			obj << "v " << m_vertices[i].x() << " " <<  totalH << " " << m_vertices[i].z() << std::endl;
		}
		for(unsigned int i = 0; i < m_faces.size(); ++i)
		{
			obj << "f " << m_faces[i].at(0) + 1 << "//" << m_faces[i].at(0) + 1
				<< " " <<  m_faces[i].at(1) + 1 << "//" << m_faces[i].at(1) + 1
				<< " " <<  m_faces[i].at(2) + 1 << "//" << m_faces[i].at(2) + 1 << std::endl;
		}
		obj.close();
	}

	std::cout << "Export terminé!" << std::endl;
}

void Terrain::exportLayersMap(const std::string name)
{
	for(uint l = 0; l < m_layers.size(); ++l)
	{
		cimg_library::CImg<double> img(m_x, m_y+1, 1, 1);
		img.channel(0);
		img.fill(0);
		for(double j = 0.; j <= m_y; j++)
		{
			for(double i = 0.; i <= m_x; i++)
			{
				img(i, j) =  atILayer(i, j, l);
			}
		}
		img.normalize(0, 255);
		std::string final = name;
		final.append("_");
		final.append(m_layers.at(l).name());
		img.mirror("xy");
		img.save(final.c_str());
	}
}


void Terrain::exportFlowMap(const std::string name)
{
	cimg_library::CImg<double> img(m_x+1, m_y+1);
	img.fill(0);
	img.channel(0);

	for(double j = 0.; j <= m_y; ++j)
	{
		for(double i = 0.; i <= m_x; ++i)
		{
			img(i,j) = 255*atI(i,j, false);
		}
	}
	img.normalize(0, 255);
	img.mirror("xy");
	img.save(name.c_str());
}


void Terrain::exportHeightMap(const std::string name)
{
	cimg_library::CImg<double> img(m_x+1, m_y+1);
	img.channel(0);
	img.fill(0);

	auto mM = std::minmax_element(m_bedRock.begin(), m_bedRock.end());
	double m = *mM.first;
	double M = *mM.second;
	double decal = 0.;
	decal = -m;
	M += decal;
	for(double j = 0.; j <= m_y; ++j)
	{
		for(double i = 0.; i <= m_x; ++i)
		{
			img(i, j) =  255*((atI(i,j) + decal )/ M);
		}
	}

	img.normalize(0, 255);
	img.mirror("xy");
	img.save(name.c_str());
}

void Terrain::exportPlantsMap(const std::string name)
{
	for(uint i = 0; i < m_species.size(); ++i)
	{
		cimg_library::CImg<int> img(m_x, m_y+1, 1, 3);
		img.fill(255);
		Disc d ;
		for(uint j = 0; j < 4; ++j)
		{
			for(Environnement::Plante plant : m_plants.at(j))
			{
				if(plant.species()->id() == i)
				{
					d = plant.disc();
					img.draw_circle(d.center().x(), d.center().y(), d.radius(), plant.species()->colors);
				}
			}
		}
		std::string final = name;
		final += "_";
		final += std::to_string(i);
		img.mirror("xy");
		img.save(final.c_str());
	}
}

double Terrain::computeTemperature(const uint &i)
{
	assert(i < m_vertices.size());
	return (AVG_TEMP - GRAD_TEMP*m_vertices.at(i).y());
}

double Terrain::computeTemperature(const double &z)
{
	return (AVG_TEMP - GRAD_TEMP*z);
}


FaceCirculator Terrain::face_incident(Vertex &v)
{
	return FaceCirculator(&m_faces, v);
}

VertexCirculator Terrain::vertex_incident(Vertex &v)
{
	return VertexCirculator(&m_faces, &m_vertices, v);
}

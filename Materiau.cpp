#include <cmath>

#include <Materiau.hpp>

Materiau::Materiau()
{}


Materiau::Materiau(const std::string &name, const double &a, const double &vd, const double &h): m_name(name), _t_angle(a), m_volumicMass(vd), m_height(h)
{}


double Materiau::volumicMass() const
{
	return this->m_volumicMass;
}

std::string Materiau::name() const
{
	return this->m_name;
}

double Materiau::height() const
{
	return this->m_height;
}

void Materiau::addHeight(const double &h)
{
	this->m_height+=h;
}

void Materiau::decreaseHeight(const double &h)
{
	this->m_height-=h;
	if(this->m_height < 0.0)
		this->m_height = 0.0;
}

void Materiau::setHeight(const double &h)
{
	this->m_height = h;
}

double Materiau::computeHeight(const double &angle)
{
	if(angle > _t_angle)
	{
		double opp = std::sin(angle*M_PI/180) / 2.0;
		return opp;
	}
	return -1;

}

bool compareVMass (const Materiau &m1, const Materiau &m2)
{
	return (m1.volumicMass() > m2.volumicMass());
}

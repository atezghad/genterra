#include <cstdlib>
#include <iostream>
#include <chrono>

#include <Terrain.hpp>
/**
* \file main.cpp
* \author Anis Tezghadanti
* */

/*! \mainpage GenTerra
 *
 * \section intro_sec Introduction
 *
 * Bienvenue dans la documentation de GenTerra. Il s'agit d'un programme servant à la conception de terrains réalistes en 3D. Le programme est capable, à l'aide de divers paramètres, de créer un terrain et de simuler son écosystème. Il est ensuite possible d'exporter le maillage obtenu afin de le retoucher sous un logiciel de modélisation 3D, ainsi que les maps associés afin, par exemple, de générer une végétation cohérente avec le terrain.
 *
 * \section install_sec Librairies requises
 * OpenMP - API dédié au multithreading<br>
 * libpng - librairie PNG pour pouvoir récupérer les différentes maps
 */

//#define IMGk
int main()
{
	auto start = std::chrono::high_resolution_clock::now();
#ifdef IMG
	Terrain t(
	{-500., -500.},
				"./myPerlin.png", -50., 500.,
	 {{"Sand",    25. , 1600., 1.}},
	 { {25, 80., 6., .8, .1, 15., 29., 3, 20.,  'F', "F[+F]F[-F]"},
	   {5, 30, 1.5, .98, .02, 20., 35.,  4, 90.,'F', "F[+F][-F]"},
	   {5, 50, 5., .5, .05, 5., 29., 2, 60., 'F', "F[+++F]F[-F]F-F[+F]"}});

	 t.exportOBJ("./test.obj");
	 t.simulation();
#else
		Terrain t(
		 {-500., -500.}, {500, 500},
		 4,4,
		 {{200., 1./1000., 120.},
		  {50.,  1./250,  10.},
		  {15.,  1./75.,  5.},
		  {5.,   1./27.,  1.},},

		 {{"Sand",    25. , 1600., 2.},
		  {"Argil",   40.,  2600.0, 1. }},

		 {{1, 10.,  1., .5, .1, 12., 17., 'F', "F[+F]F[-F]",  0, 20.},
		  {1, 15., 1.5, .4, .04, 14., 20., 'F', "F[+F]F[-F]",  0, 37.},
		  {1, 14.,  .5, .6, .06, 15., 18., 'F', "F[+F]F[-F]",  1, 30.}
		 });

		t.simulation();
		t.exportOBJ("Terrain.obj");
		t.exportHeightMap("Heightmap.png");
		t.exportLayersOBJ("Layer.obj");
		t.exportLayersMap("Layer.png");
		t.exportFlowMap("FlowMap.png");
		t.exportPlantsOBJ("Plants.obj");
		t.exportPlantsMap("Plants.png");
#endif
	auto finish = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> elapsed = finish - start;
	std::cout << "Elapsed time: " << elapsed.count() << " s" << std::endl;
	return EXIT_SUCCESS;
}

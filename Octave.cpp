#include <iostream>
#include <random>

#include <Octave.hpp>
#include <Utils/Random.hpp>

Octave::Octave() : amplitude(1)
{
}

Octave::Octave(const double amp, const double f, const double t) : amplitude(amp), threshold(t)
{
	bruit.SetOctaveCount(1);
	bruit.SetFrequency(f);

	angle = randomDouble(0, 360)*M_PI/180;
	c_angle = std::cos(angle);
	s_angle = std::sin(angle);
	shift = randomDouble(0,1);
}


double Octave::getValue(const double x, const double y)
{
	double x_r = (x * c_angle - y * s_angle) + shift;
	double y_r = (x * s_angle + y * c_angle) + shift;
	double res = amplitude*bruit.GetValue(x_r, y_r, 0);
	res = (res > threshold)? 2*threshold - res : res;
	return res;
}

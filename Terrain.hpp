#ifndef TERRAIN_H
#define TERRAIN_H

#include <vector>
#include <string>
#include <initializer_list>
#include <map>
#include <array>

#include <Utils/Coords2D.hpp>
#include <Utils/Vertex.hpp>
#include <Utils/Vec3D.hpp>
#include <Utils/Triangle.hpp>

#include <Environnement.hpp>
#include <Octave.hpp>
#include <Materiau.hpp>

/**
* \file Terrain.hpp
* \author Anis Tezghadanti
* */

class FaceCirculator;
class VertexCirculator;
typedef std::pair<int, int> Cle;

/**
 * \class Terrain
 * \brief Classe Terrain
 * Cette classe contient toutes les fonctions avec lequels l'utilisateur interagit pour créer le terrain de ses rêves
 * */

class Terrain
{

	public:
		/**
		 * \brief Constructeur de l'objet Terrain
		 * \param a Extremité inférieure gauche du terrain
		 * \param b Extremité supérieure droite du terrain
		 * \param stepX Pas d'échantillonage en largeur du terrain
		 * \param stepY Pas d'échantillonage en longeur du terrain
		 * \param list_octaves Liste des différents niveaux de bruits appliqués au terrain
		 * \param list_mat Liste des différentes couches de matériaux
		 * \param list_species Liste des différentes espèces de végétaux
		 * */
		Terrain(const Coords2D &a, const Coords2D &b,
				const uint &stepX, const uint &stepY,
				std::initializer_list<Octave> list_octaves,
				std::initializer_list<Materiau> list_mat,
				std::initializer_list<Environnement::Espece> list_species);

		/**
		 * \brief Constructeur de l'objet Terrain
		 * \param img_filename Chemin vers l'image à charger
		 * \param a Extremité inférieure gauche du terrain
		 * \param minZ Altitude minimum désirée
		 * \param maxZ Altitude maximum désirée
		 * \param list_mat Liste des différentes couches de matériaux
		 * \param list_species Liste des différentes espèces de végétaux
		 * */
		Terrain(const Coords2D &a,
				const std::string& img_filename, double minY, double maxY,
				std::initializer_list<Materiau> list_m,
				std::initializer_list<Environnement::Espece> list_species);

		/**
		 * \brief Renvoie l'altitude en un point
		 * \param p Le point dont on souhaite obtenir l'altitude
		 * \return L'altitude au point p(x,y)
		 * */
		double getY(const Coords2D &p) ;

		/**
		 * \brief Ajoute une nouvelle octave de bruit
		 * \param amp La valeur maximum désirée
		 * \param f La fréquence
		 * \param t La valeur de seuil
		 * */
		void addOctave(const double &amp, const double &f, const double &t);

		/**
		 * \brief Exporte le maillage du Terrain au format WaveFront(.OBJ)
		 * \param filename L'emplacement et le nom du fichier
		 * */
		void exportOBJ(const std::string filename);

		/**
		 * \brief Exporte le maillage des couches du Terrain au format WaveFront(.OBJ)
		 * \param filename L'emplacement du fichier (le nom et l'extension sont générés automatiquement
		 * */
		void exportLayersOBJ(const std::string filename);
		/**
		 * \brief Exporte les maps des différentes couches de matériaux au format .png
		 * \param name L'emplacement et le nom du fichier
		 * */
		void exportLayersMap(const std::string name);

		/**
		 * \brief Exporte la heightmap au format .png
		 * \param name L'emplacement et le nom du fichier
		 * */
		void exportHeightMap(const std::string name);

		/**
		 * \brief Exporte la flowmap au format .png
		 * \param name L'emplacement et le nom du fichier
		 * */
		void exportFlowMap(const std::string name);

		/**
		 * \brief Exporte les différentes maps des espèces végétales au format .png
		 * \param name L'emplacement et le nom du fichier
		 * */
		void exportPlantsMap(const std::string name);

		/**
		 * \brief Exporte les différentes maps des espèces végétales au format .OBJ
		 * \param name L'emplacement et le nom du fichier
		 * */
		void exportPlantsOBJ(const std::string name);

		/**
		 * \brief Exporte toues les maps et tous les .OBJ
		 * \param name L'emplacement
		 * */
		void exportAll(const std::string name);

		/**
		 * \brief Simule un pas de temps au niveau de l'environnement du Terrain (déplacement des couches, ecosystème...)
		 * \param s Le nombre de pas de temps (1 par défaut)
		 * */
		void simulation(const uint &s = 1);

		/**
		 * \brief Simule un pas de temps au niveau d'une couche de matériau
		 * \param m L'indice du matériau
		 * */
		void simulateLayer(const int &m);
		/**
		 * \brief Simule un pas de temps au niveau de la simulation d'écoulement
		 * */
		void simulateFlow();
		/**
		 * \brief Simule un pas de temps au niveau de l'écosystème du terrain
		 * */
		void simulateEcoSystem();

		/**
		 * \brief Simule l'érosion hydrique sur le Terrain [INCOMPLET]
		 * */
		void erodeHydro();


	protected:

		std::vector<Octave> m_octaves;
		std::vector<Triangle> m_faces;
		std::vector<Vertex> m_vertices;
		std::vector<Vec3D> m_normals;
		std::vector<Materiau> m_layers;
		std::vector<Materiau> m_layers_vertices;
		std::vector<Environnement::Espece> m_species;
		std::array< std::list<Environnement::Plante>, 4> m_plants;
		std::vector<double> m_flow;
		std::vector<double> m_bedRock;

		Coords2D m_origin;
		Coords2D m_arrival;
		uint m_step_x;
		uint m_step_y;
		uint m_x;
		uint m_y;

	private:
		static int triangleCount;
		std::map<Cle, Cle> m_adjacencies;
		double computeTemperature(const uint &i);
		double computeTemperature(const double &z);

		void init(std::initializer_list<Materiau> &list_m,
				  std::initializer_list<Environnement::Espece> &list_species, const bool t);
		void computeY();
		void computePoints(const bool t=true);
		void computeFaces();
		void computeNormalFaces();
		void computeNormalVertices();
		void computeNeighbors();
		void computePlants();
		void addAdjacency(Triangle &t, const int &p1, const int &p2, const int &i);
		void addAdjacencies(Triangle &t, const int &i);
		void setIncidents(Triangle &t, const int &i);
		void addFace(Triangle &t);
		bool addPlant(const uint &id, const double &x, const double &y, const double o = 0., const bool root=false);
		int nextPoint(const int &i);
		int whichRegion(const double &x, const double &y);
		uint at(const int i, const int j);
		double atI(const double x, const double y, const bool h=true);
		double atILayer(const double x, const double y, const int m);


		FaceCirculator face_incident(Vertex &v);
		VertexCirculator vertex_incident(Vertex &v);
};

#include <Utils/FaceCirculator.hpp>
#include <Utils/VertexCirculator.hpp>

#endif // TERRAIN_H

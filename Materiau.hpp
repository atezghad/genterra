#ifndef MATERIAU_H
#define MATERIAU_H

#include <string>
#include <initializer_list>
/**
* \file Materiau.hpp
* \author Anis Tezghadanti
* */

/**
 * \class Materiau
 * \brief Classe Materiau
 * Cette classe permet de gérer un type de matériau
 * */
class Materiau
{
	public:
		/**
		 * \brief Constructeur de l'objet Materiau par défaut
		 * */
		Materiau();

		/**
		 * \brief Constructeur de l'objet Materiau
		 * \param name Le nom du matériau (sert à l'export en .OBJ)
		 * \param a L'angle de talus
		 * \param vd La masse volumique
		 * \param h La hauteur de départ
		 * */
		Materiau(const std::string &name, const double &a, const double &vd, const double &h);

		/**
		 * \brief Retourne la masse volumique du Materiau
		 * \return La masse volumique du Materiau
		 * */
		double volumicMass() const;

		/**
		 * \brief Retourne le nom du Materiau
		 * \return Le nom du Materiau
		 * */
		std::string name() const;

		/**
		 * \brief Retourne la hauteur du matériau
		 * \return La hauteur du Materiau
		 * */
		double height() const;

		/**
		 * \brief Augmente la hauteur du Materiau
		 * \param h La valeur ajoutée
		 * */
		void addHeight(const double &h);

		/**
		 * \brief Diminue la hauteur du Materiau
		 * \param h La valeur diminuée
		 * */
		void decreaseHeight(const double &h);

		/**
		 * \brief Modifie la hauteur du Materiau
		 * \param h La nouvelle hauteur
		 * */
		void setHeight(const double &h);

		/**
		 * \brief Calcule l'excédent de la hauteur en fonction de l'angle
		 * \param angle La pente
		 * \return L'excédent de la hauteur si angle> angle de talus, -1 sinon
		 * */
		double computeHeight(const double &angle);

	protected:
		std::string m_name;
		double _t_angle;
		double m_volumicMass;
		double m_height;
};

bool compareVMass (const Materiau &m1, const Materiau &m2);
#endif // MATERIAU_H

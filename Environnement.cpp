#include <cmath>
#include <random>
#include <stack>
#include <tuple>

#include <Environnement.hpp>
#include <Utils/Random.hpp>

Environnement::Espece::Espece(const uint &newPlants, const double &maxSize,
							  const double &averageGrowth, const double &probaComp,
							  const double &wetOrDry, const double &minTemp,
							  const double &maxTemp,
							  const char symbol, const std::string rule, const uint &nbIter, const double &angle) :
	_newPlants(newPlants), _maxSize(maxSize),
	_averageGrowth(averageGrowth), _probaComp(probaComp),
	_wetOrDry(wetOrDry),
	_minTemp(minTemp),_maxTemp(maxTemp),
	_angle(angle), _symbol(symbol)
{
	for(uint i = 0; i < 3; ++i)
	{
		colors[i] = randomInt(0, 255);
	}
	_dx = randomDouble(-maxSize, maxSize);
	_dy = randomDouble(-maxSize, maxSize);
	_rule = LRule(nbIter, symbol, rule);
	_arborescence = _rule.getCurrent();
}

double Environnement::Espece::maxTemp() const
{
	return _maxTemp;
}

double Environnement::Espece::minTemp() const
{
	return _minTemp;
}

double Environnement::Espece::wetOrDry() const
{
	return _wetOrDry;
}

double Environnement::Espece::probaComp() const
{
	return _probaComp;
}

double Environnement::Espece::averageGrowth() const
{
	return _averageGrowth;
}

double Environnement::Espece::maxSize() const
{
	return _maxSize;
}

uint Environnement::Espece::newPlants() const
{
	return _newPlants;
}

void Environnement::Espece::setId(uint id)
{
	_id = id;
}

uint Environnement::Espece::id() const
{
	return _id;
}

char Environnement::Espece::symbol() const
{
	return _symbol;
}

int Environnement::Espece::dy() const
{
	return _dy;
}

int Environnement::Espece::dx() const
{
	return _dx;
}

double Environnement::Espece::angle() const
{
	return _angle;
}

std::string Environnement::Espece::arborescence() const
{
	return _arborescence;
}

double Environnement::Plante::VIGOR_MIN = .0;

Environnement::Plante::Plante(Environnement::Espece &species, const double &vigor, const double &x, const double &y, const double &orientation, const bool &root) :
	_species(&species), _vigor(vigor), _disc(1., Coords2D(x,y)),_competitivity(1.0),
	_age(0), _orientation(orientation), _root(root), _old(1)
{
}

void Environnement::Plante::setDisc(const Disc &disc)
{
	_disc = disc;
}

Disc Environnement::Plante::disc() const
{
	return _disc;
}

bool Environnement::Plante::operator==(const Environnement::Plante &p) const
{
	return (_species->id() == p._species->id()
			&& _disc.center().x() == p._disc.center().x()
			&& _disc.center().y() == p._disc.center().y());
}

bool Environnement::Plante::operator!=(const Environnement::Plante &p) const
{
	return !(*this == p);
}

void Environnement::Plante::setVigor(double &vigor)
{
	_vigor = vigor;
}

double Environnement::Plante::vigor() const
{
	return _vigor;
}

void Environnement::Plante::setSpecies(Environnement::Espece &species)
{
	_species = &species;
}

Environnement::Espece* Environnement::Plante::species() const
{
	return _species;
}

bool Environnement::Plante::competition(const Environnement::Plante &p)
{
	return(_disc.intersect(p._disc));
}

double Environnement::Plante::competitivity() const
{
	return _competitivity;
}

offspring Environnement::Plante::spread()
{
	std::string current = _species->arborescence();
	offspring to_spread;
	Coords2D last = _disc.center();
	double sp_angle = _species->angle();
	double curr_angle = _orientation;
	int nb_open = 0;
	std::stack<std::pair<Coords2D, double> > stack;

	for(uint i = 0; i < current.length(); ++i)
	{
		if(current.at(i) == _species->symbol())
		{
			double c_angle = std::cos(curr_angle*M_PI/180.);
			double s_angle = std::sin(curr_angle*M_PI/180.);

			int x_r = _species->dx() * c_angle - _species->dy() * s_angle;
			int y_r = _species->dx() * s_angle + _species->dy() * c_angle;

			x_r +=last.x();
			y_r +=last.y();
			bool root = false;
			last = Coords2D(x_r, y_r);
			if(nb_open == 0)
			{
				root = true;
			}
			std::tuple<Coords2D, double, bool, uint> to_insert(last, curr_angle, root, _species->id());
			to_spread.push_back(to_insert);
		}
		else if(current.at(i) == '+')
		{
			curr_angle+= std::fmod(sp_angle, 360.);
		}
		else if(current.at(i) == '-')
		{
			curr_angle-= std::fmod(sp_angle, 360.);
		}
		else if(current.at(i) == '[')
		{
			nb_open++;
			stack.push(std::pair<Coords2D, double>(last, curr_angle));
		}
		else if(current.at(i) == ']')
		{
			std::pair<Coords2D, double> p = stack.top();
			last = p.first;
			curr_angle = p.second;
			stack.pop();
			nb_open--;
		}
	}
	return to_spread;
}

void Environnement::Plante::setRoot(const bool &root)
{
	_root = root;
}

bool Environnement::Plante::root() const
{
	return _root;
}

void Environnement::Plante::grow(const double humidity, const double temperature)
{
	if(_vigor != Environnement::Plante::VIGOR_MIN)
	{
		++_age;
		if(_disc.radius() >= _species->maxSize())
		{
			//_vigor = 0.0;
			_old = -1;
		}
		else
		{
			double id_temp = (_species->minTemp() + _species->maxTemp()) / 2.;
			double temp_now = std::exp(-((temperature - id_temp)*(temperature - id_temp)) / 2.);
			double humid_now = std::exp(-((humidity - _species->wetOrDry())*(humidity - _species->wetOrDry())) / 2.);
			_disc.increaseRadius(_old*_species->averageGrowth()*temp_now*humid_now);
			_vigor*=(temp_now*humid_now);
			_competitivity = _vigor*(_disc.radius() / _species->maxSize());

		}
	}
}

#ifndef VERTEXCIRCULATOR_H
#define VERTEXCIRCULATOR_H

#include <vector>
#include <list>

#include <Utils/Vertex.hpp>
#include <Utils/FaceCirculator.hpp>

class VertexCirculator
{
public:
	VertexCirculator();
	VertexCirculator(const VertexCirculator &f);
	VertexCirculator(VertexCirculator &&f);
	VertexCirculator(std::vector<Triangle> *triangles, std::vector<Vertex> *points, Vertex &p);

	VertexCirculator& operator=(const VertexCirculator &e);
	bool operator!=(const VertexCirculator &e) const;
	bool operator==(const VertexCirculator &e) const;
	Vertex& operator*();
	VertexCirculator& operator++();

protected:

	unsigned int _counter;
	FaceCirculator it_fcircator;
	std::vector<Triangle> *ptr_tri;
	std::vector<Vertex> *ptr_points;
	std::vector<int> pts_adjacents;
	Vertex * ref;
	Vertex * val;

private:
	void findVertex();
};

#endif // VERTEXCIRCULATOR_H

#ifndef VERTEX_H
#define VERTEX_H

#include <list>

#include <Utils/Coords3D.hpp>
#include <Utils/Vec3D.hpp>

class Vertex : public Coords3D
{
	public:
		Vertex();
		Vertex(const double &a, const double &b, const double &c);
		Vertex(const double &a, const double &b, const double &c, const int &i);
		Vertex(const double &a, const double &b, const double &c, const int &t, const int &i) ;

		void operator =(Vertex &v);
		bool operator==(Vertex &v);
		bool operator!=(Vertex &v);

		Vec3D normal() const;
		void setNormal(Vec3D &n);
		int index() const;
		int incident() const;
		void setIncident(const int &i);
		void setIndex(int i);
		void addNeighbor(const int &n);
		std::list<int> neighbors() const;

	protected:
		Vec3D _normal;
		std::list<int> _neighbors;
		int _index;
		int tri_incident;
};

#endif // VERTEX_H

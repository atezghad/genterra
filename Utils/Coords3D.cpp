#include <Utils/Coords3D.hpp>

Coords3D::Coords3D() : _x(0), _y(0), _z(0)
{}

Coords3D::Coords3D(const double &a, const double &b, const double &c) : _x(a), _y(b), _z(c)
{}


double Coords3D::x() const
{
	return this->_x;
}

double Coords3D::y() const
{
	return this->_y;
}

double Coords3D::z() const
{
	return this->_z;
}

void Coords3D::setX(double x)
{
    _x = x;
}

void Coords3D::setY(double y)
{
    _y = y;
}

void Coords3D::setZ(double z)
{
    _z = z;
}

void Coords3D::operator =(const Coords3D &v)
{
	this->_x = v._x;
	this->_y = v._y;
	this->_z = v._z;
}

bool Coords3D::operator==(const Coords3D &v) const
{
	return (this->_x == v._x && this->_y == v._y && this->_z == v._z);
}

bool Coords3D::operator!=(const Coords3D &v) const
{
	return (!(*this == v));
}

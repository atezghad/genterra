#include <algorithm>

#include <Utils/Vertex.hpp>

Vertex::Vertex() : Coords3D(0,0,0),_normal(0,0,0), _index(-1), tri_incident(-1)
{}

Vertex::Vertex(const double &a, const double &b, const double &c)
	: Coords3D(a,b,c),_normal(0,0,0), _index(-1), tri_incident(-1)
{}

Vertex::Vertex(const double &a, const double &b, const double &c, const int &i)
	: Coords3D(a,b,c),_normal(0,0,0), _index(i), tri_incident(-1)
{}

Vertex::Vertex(const double &a, const double &b, const double &c, const int &t, const int &i)
	: Coords3D(a,b,c),_normal(0,0,0), _index(i), tri_incident(t)
{}

void Vertex::operator =(Vertex &v)
{
	this->_x = v._x;
	this->_y = v._y;
	this->_z = v._z;
	this->_normal = v._normal;
	this->_index = v._index;
	this->tri_incident = v.tri_incident;
	this->_neighbors = v._neighbors;
}

bool Vertex::operator==(Vertex &v)
{
	return (this->_x == v._x && this->_y == v._y
			&& this->_z == v._z
			&& this->_normal == v._normal
			&& this->tri_incident == v.tri_incident
			&& this->_index == v._index
			);
}

bool Vertex::operator!=(Vertex &v)
{
	return (!(*this == v));
}

Vec3D Vertex::normal() const
{
	return this->_normal;
}

void Vertex::setNormal(Vec3D &n)
{
	this->_normal = n;
}

int Vertex::incident() const
{
	return this->tri_incident;
}

void Vertex::addNeighbor(const int &n)
{
	if(std::find(this->_neighbors.begin(), this->_neighbors.end(), n) == this->_neighbors.end())
	{
		this->_neighbors.push_back(n);
	}
}

std::list<int> Vertex::neighbors() const
{
	return this->_neighbors;
}


void Vertex::setIncident(const int &i)
{
	this->tri_incident = i;
}

int Vertex::index() const
{
	return _index;
}

void Vertex::setIndex(int i)
{
	_index = i;
}

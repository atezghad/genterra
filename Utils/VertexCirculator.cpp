#include <cstdlib>
#include <algorithm>

#include <Utils/VertexCirculator.hpp>

VertexCirculator::VertexCirculator():_counter(1), it_fcircator(), ptr_tri(nullptr),
	ptr_points(nullptr), ref(nullptr), val(nullptr) {}

VertexCirculator::VertexCirculator(const VertexCirculator &f)
{
	this->_counter = f._counter;
	this->it_fcircator = f.it_fcircator;
	this->ptr_tri = f.ptr_tri;
	this->ptr_points = f.ptr_points;
	this->pts_adjacents = f.pts_adjacents;
	this->ref = f.ref;
	this->val = f.val;
}

VertexCirculator::VertexCirculator(VertexCirculator &&f)
{
	this->_counter = f._counter;
	this->it_fcircator = f.it_fcircator;
	this->ptr_tri = f.ptr_tri;
	this->ptr_points = f.ptr_points;
	this->pts_adjacents = f.pts_adjacents;
	this->ref = f.ref;
	this->val = f.val;
	f._counter = 1;
	f.ptr_tri = nullptr;
	f.ptr_points = nullptr;
	f.pts_adjacents.clear();
	f.ref = nullptr;
	f.val = nullptr;
}

void VertexCirculator::findVertex()
{
	this->pts_adjacents = (*it_fcircator).get_points();
	std::vector<int>::iterator it2 = std::find(this->pts_adjacents.begin(),
											  this->pts_adjacents.end(),
											  this->ref->index());
	std::vector<int>::iterator n;
	if(*it2 == this->pts_adjacents[2])
		n = std::next(this->pts_adjacents.begin(), this->_counter-1);
	else if(*it2 == this->pts_adjacents[1] && this->_counter == 2)
		n = this->pts_adjacents.begin();
	else
		n = std::next(it2, this->_counter);
	if(n == this->pts_adjacents.end())
		n = this->pts_adjacents.begin();
	this->val = &(this->ptr_points->at(*n));
}

VertexCirculator::VertexCirculator(std::vector<Triangle> *triangles,
								   std::vector<Vertex> *points,
								   Vertex &p)
{
	this->_counter = 1;
	this->it_fcircator = FaceCirculator(triangles, p);
	this->ptr_tri = triangles;
	this->ptr_points = points;
	this->ref = &p;
	this->findVertex();
}

VertexCirculator& VertexCirculator::operator=(const VertexCirculator &f)
{
	this->_counter = f._counter;
	this->it_fcircator = f.it_fcircator;
	this->ptr_tri = f.ptr_tri;
	this->ptr_points = f.ptr_points;
	this->pts_adjacents = f.pts_adjacents;
	this->ref = f.ref;
	this->val = f.val;
	return *this;
}

bool VertexCirculator::operator==(const VertexCirculator &e) const
{
	return (this->ptr_tri == e.ptr_tri && this->ptr_points == e.ptr_points
			&& this->it_fcircator == e.it_fcircator && this->val == e.val
			&& this->ref == e.ref && this->_counter == e._counter);
}

bool VertexCirculator::operator!=(const VertexCirculator &e) const
{
	return(!(*this == e) );
}

Vertex& VertexCirculator::operator*()
{
	return *(this->val);
}

VertexCirculator& VertexCirculator::operator++()
{
	if(this->_counter == 1)
	{
		++this->_counter;
	}
	else
	{
		++(this->it_fcircator);
	}
	if(&(*(this->it_fcircator)) != nullptr)
		findVertex();
	return *this;
}

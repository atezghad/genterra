#ifndef RANDOMLIB_HPP
#define RANDOMLIB_HPP

#include <random>

int randomInt(const double a, const double b);
double randomDouble(const double a, const double b);

#endif // RANDOMLIB_HPP

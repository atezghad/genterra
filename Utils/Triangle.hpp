#ifndef TRIANGLE_H
#define TRIANGLE_H

#include <vector>
#include <list>

class Triangle
{
	protected:
		int _index;
		std::vector<int> points;
		std::vector<int> adjacents;

	public:
		Triangle(const unsigned int &a, const unsigned int &b, const unsigned int &c);
		Triangle(const unsigned int &a, const unsigned int &b, const unsigned int &c, const int &i);
		Triangle(const Triangle &t);
		Triangle(Triangle &&t);

		int at(const int &i);
		int index() const;
		void setIndex(const int &i)	;
		void ajouter_point(const int &p);
		void ajouter_adjacent(const int &p);
		void supprimer_adjacent(const int &remove);
		std::vector<int> get_adjacents();
		std::vector<int> get_points();
		std::list<int> common(const Triangle &t);

		Triangle& operator =(const Triangle &t);
		bool operator==(Triangle &t);
		bool operator!=(Triangle &t);

};

#endif // TRIANGLE_H

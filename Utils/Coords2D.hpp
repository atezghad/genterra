#ifndef POINT_H
#define POINT_H


class Coords2D
{
	public:
		Coords2D();
		Coords2D(const double x, const double y);
		double x() const;
		double y() const;
		double distanceSq(const Coords2D &c);
		double distance(const Coords2D &c);

	protected:
		double m_x;
		double m_y;
};

#endif // POINT_H

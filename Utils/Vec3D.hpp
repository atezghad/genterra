#ifndef VEC3D_H
#define VEC3D_H

#include <Utils/Coords3D.hpp>

class Vec3D : public Coords3D
{
	public:
		static Vec3D X;
		static Vec3D Y;
		static Vec3D Z;

		Vec3D();
		Vec3D(Coords3D &a, Coords3D &b);
		Vec3D(const double &a, const double &b, const double &c);

		Vec3D scalar(const double &d);
		double scalar(const Vec3D &v);
		Vec3D operator -();
		Vec3D operator +(const Vec3D &v);
		void operator +=(const Vec3D &v);

		double angleRad(const Vec3D &v);
		double angleDeg(const Vec3D &v);
		Vec3D cross(const Vec3D &v);
		double norm() const;
		void normalize();
};

#endif // VEC3D_H

#ifndef DISC_H
#define DISC_H

#include <Utils/Coords2D.hpp>

class Disc
{
	public:
		Disc();
		Disc(const double rayon, Coords2D center);
		bool in(const Coords2D p);
		bool intersect(const Disc &d);
		void increaseRadius(const double &r);

		double radius() const;
		Coords2D center() const;

	protected:
		double _radius;
		Coords2D _center;
};

#endif // DISC_H

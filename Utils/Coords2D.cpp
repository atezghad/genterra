#include <cmath>

#include <Utils/Coords2D.hpp>

Coords2D::Coords2D() : m_x(0.0), m_y(0.0){}

Coords2D::Coords2D(const double x, const double y) : m_x(x), m_y(y){}

double Coords2D::x() const
{
	return m_x;
}

double Coords2D::y() const
{
	return m_y;
}

double Coords2D::distanceSq(const Coords2D &c)
{
	double mag_x = std::abs(m_x - c.m_x);
	mag_x*=mag_x;
	double mag_y = std::abs(m_y - c.m_y);
	mag_y*=mag_y;

	return mag_x+mag_y;
}

double Coords2D::distance(const Coords2D &c)
{
	return std::sqrt(distanceSq(c));
}

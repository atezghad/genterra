#include <cmath>

#include <Utils/Disc.hpp>

Disc::Disc() : _radius(0), _center(Coords2D(0,0))
{
}

Disc::Disc(const double rayon, Coords2D center) : _radius(rayon), _center(center)
{
}

bool Disc::in(const Coords2D p)
{
	return (_center.distanceSq(p) <= _radius*_radius);
}

bool Disc::intersect(const Disc &d)
{
	double distanceToP = _center.distance(d._center);
	distanceToP -= (_radius + d._radius);
	return (distanceToP <= 0.0);
}

void Disc::increaseRadius(const double &r)
{
	_radius+=r;
}

double Disc::radius() const
{
	return _radius;
}

Coords2D Disc::center() const
{
	return _center;
}

#ifndef COORDS_H
#define COORDS_H


class Coords3D
{
	public:
		Coords3D();
		Coords3D(const double &a, const double &b, const double &c);

		virtual void operator =(const Coords3D &v);
		virtual bool operator==(const Coords3D &v) const ;
		virtual bool operator!=(const Coords3D &v) const;

		double x() const;
		double y() const;
		double z() const;

		void setX(double x);
		void setY(double y);
		void setZ(double z);

	protected:
		double _x;
		double _y;
		double _z;

};

#endif // COORDS_H

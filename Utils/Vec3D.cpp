#include <cmath>

#include <Utils/Vec3D.hpp>

Vec3D Vec3D::X(1,0,0);
Vec3D Vec3D::Y(0,1,0);
Vec3D Vec3D::Z(0,0,1);

Vec3D::Vec3D() : Coords3D(0,0,0)
{
}

Vec3D::Vec3D(Coords3D &a, Coords3D &b) : Coords3D(b.x()-a.x(),
												  b.y()-a.y(),
												  b.z()-a.z())
{
}

Vec3D::Vec3D(const double &a, const double &b, const double &c): Coords3D(a,b,c)
{
}



Vec3D Vec3D::scalar(const double &d)
{
	return Vec3D(d*this->_x,
				 d*this->_y,
				 d*this->_z);
}

double Vec3D::scalar(const Vec3D &v)
{
	return this->_x * v._x+
			this->_y * v._y+
			this->_z * v._z;
}

Vec3D Vec3D::operator-()
{
	return this->scalar(-1.);
}

Vec3D Vec3D::operator +(const Vec3D &v)
{
	return Vec3D(this->_x + v._x,
				 this->_y + v._y,
				 this->_z + v._z);
}

void Vec3D::operator +=(const Vec3D &v)
{
	*this = *this + v;
}

double Vec3D::angleRad(const Vec3D &v)
{
	double s = scalar(v);
	double n = norm() * v.norm();
	return std::acos(s/n);
}

double Vec3D::angleDeg(const Vec3D &v)
{
	return this->angleRad(v) * 180 / M_PI;
}

Vec3D Vec3D::cross(const Vec3D &v)
{
	double x = v._x;
	double y = v._y;
	double z = v._z;
	return Vec3D(this->_y*z-this->_z*y,
				 this->_z*x-this->_x*z,
				 this->_x*y-this->_y*x
				 );

}

double Vec3D::norm() const
{
	return std::sqrt(this->_x*this->_x +
					 this->_y*this->_y +
					 this->_z*this->_z);
}

void Vec3D::normalize()
{
	double n = this->norm();
	if(n != 0.)
		*this = scalar(1.0/n);
}

#ifndef FACECIRCULATOR_H
#define FACECIRCULATOR_H

#include <Utils/Triangle.hpp>
#include <Utils/Vertex.hpp>

class FaceCirculator
{
	public:
		FaceCirculator();
		FaceCirculator(const FaceCirculator &f);
		FaceCirculator(FaceCirculator &&f);
		FaceCirculator(std::vector<Triangle> *triangles, const Vertex& v);
		FaceCirculator& operator=(const FaceCirculator &e);
		bool operator!=(const FaceCirculator &e) const;
		bool operator==(const FaceCirculator &e) const;
		Triangle& operator*();
		FaceCirculator& operator++();


	protected:
		int ref;
		std::list<int> visited;
		std::vector<Triangle> * ptr;
		Triangle * val;

};


#endif // FACECIRCULATOR_H

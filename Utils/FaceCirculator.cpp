#include <algorithm>
#include <cstdlib>
#include <iostream>

#include <Utils/FaceCirculator.hpp>

FaceCirculator::FaceCirculator():ref(0),ptr(nullptr), val(nullptr) {}

FaceCirculator::FaceCirculator(const FaceCirculator &f)
{
	this->ref = 0;
	this->val = f.val;
	this->ptr = f.ptr;
	this->visited = f.visited;
}

FaceCirculator::FaceCirculator(FaceCirculator &&f)
{
	this->ref = 0;
	this->ptr = f.ptr;
	this->val = f.val;
	this->visited = f.visited;
	f.ref = 0;
	f.ptr = nullptr;
	f.val = nullptr;
}

FaceCirculator::FaceCirculator(std::vector<Triangle> *triangles, const Vertex &v)
{
	this->ptr = triangles;
	this->ref = v.index();
	this->val = &(triangles->at(v.incident()));
	this->visited.push_back(this->val->index());

}

FaceCirculator& FaceCirculator::operator=(const FaceCirculator &e)
{
	this->ref = e.ref;
	this->ptr = e.ptr;
	this->val = e.val;
	this->visited = e.visited;
	return *this;
}

bool FaceCirculator::operator==(const FaceCirculator &e) const
{
	return (this->ref == e.ref && this->ptr == e.ptr && this->val == e.val);
}

bool FaceCirculator::operator!=(const FaceCirculator &e) const
{
	return(!(*this == e) );
}

Triangle& FaceCirculator::operator*()
{
	return *(this->val);
}

FaceCirculator& FaceCirculator::operator++()
{
	unsigned int i = 0;
	int next = 0;
	std::vector<int>::iterator it, pv, it1, it2;
	std::vector<int> pts = this->val->get_points();
	it = std::find(pts.begin(), pts.end(), this->ref);
	if(*it == pts[0])
		pv = std::prev(pts.end());
	else pv = std::prev(it);

	int second = *pv;
	bool trouve = false;
	std::vector<int> adj = this->val->get_adjacents();
	while(!trouve && i < adj.size())
	{
		Triangle t = this->ptr->at(adj[i]);
		if(std::find(this->visited.begin(), this->visited.end(), t.index()) == this->visited.end())
		{
			std::vector<int> p_list = t.get_points();
			it1 = std::find(p_list.begin(), p_list.end(), ref);
			it2 = std::find(p_list.begin(), p_list.end(), second);
			if(it1 != p_list.end() && it2 != p_list.end())
			{
				trouve = true;
				next = adj[i];
				this->visited.push_back(next);
			}
		}
		++i;
	}
	this->val = (trouve)? ( &(this->ptr->at(next)) ) : (nullptr);
	return *this;

}

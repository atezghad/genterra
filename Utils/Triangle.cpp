#include <vector>
#include <utility>
#include <algorithm>

#include <Utils/Triangle.hpp>

Triangle::Triangle(const unsigned int &a, const unsigned int &b, const unsigned int &c): _index(-1)
{
	points.reserve(3);
	adjacents.reserve(3);
	points.push_back(a);
	points.push_back(b);
	points.push_back(c);
}

Triangle::Triangle(const unsigned int &a, const unsigned int &b, const unsigned int &c, const int &i) : _index(i)
{
	points.reserve(3);
	adjacents.reserve(3);
	points.push_back(a);
	points.push_back(b);
	points.push_back(c);
}

Triangle::Triangle(const Triangle &t)
{
	_index = t._index;
	points = t.points;
	adjacents = t.adjacents;
}

Triangle::Triangle(Triangle &&t)
{
	_index = t._index;
	t._index = 0;
	points.reserve(3);
	adjacents.reserve(3);
	points = std::move(t.points);
	adjacents = std::move(adjacents);
}

int Triangle::at(const int &i)
{
	if(i < 3)
		return points.at(i);
	else return -1;
}

int Triangle::index() const
{
	return _index;
}

void Triangle::setIndex(const int &i)
{
	_index = i;
}

void Triangle::ajouter_point(const int &p)
{
	if(points.size() < 3)
		points.push_back(p);
}

void Triangle::ajouter_adjacent(const int &p)
{
	if(adjacents.size() < 3)
		adjacents.push_back(p);
}

void Triangle::supprimer_adjacent(const int &remove)
{
	for(auto i = adjacents.begin(); i != adjacents.end(); ++i)
	{
		if(*i == remove)
		{
			adjacents.erase(i);
			break;
		}
	}
}

std::vector<int> Triangle::get_adjacents()
{
	return adjacents;
}

std::vector<int> Triangle::get_points()
{
	return points;
}

Triangle& Triangle::operator =(const Triangle &t)
{
	points = t.points;
	adjacents = t.adjacents;
	return *this;
}

bool Triangle::operator ==(Triangle &t)
{
	return (this->points[0] == t.points[0]
			&& this->points[1] == t.points[1]
			&& this->points[2] == t.points[2]
			&& this->adjacents[0] == t.adjacents[0]
			&& this->adjacents[1] == t.adjacents[1]
			&& this->adjacents[2] == t.adjacents[2]
			&& this->_index == t._index
			);
}

bool Triangle::operator !=(Triangle &t)
{
	return (!(*this == t ));
}

std::list<int> Triangle::common(const Triangle &t)
{
	std::list<int> list;
	for(auto it = this->points.begin(); it != this->points.end();++it)
	{
		for(auto it2 = t.points.begin(); it2 != t.points.end();++it2)
		{
			if(*it == *it2)
			{
				list.push_back(*it);
			}
		}
	}
	return list;
}

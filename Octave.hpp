#ifndef NOISE_H
#define NOISE_H

#include <vector>

#include <znoise/src/module/perlin.h>
/**
* \file Octave.hpp
* \author Anis Tezghadanti
* */

/**
 * \class Octave
 * \brief Classe Octave
 * Cette classe permet de gérer un niveau de bruit de Perlin
 * */
class Octave
{
	public:
		/**
		 * \brief Constructeur de l'objet Octave par défaut
		 * */
		Octave();

		/**
		 * \brief Constructeur de l'objet Octave
		 * \param amp La valeur maximum désirée
		 * \param f La fréquence
		 * \param t La valeur de seuil
		 * */
		Octave(const double amp, const double f, const double t);
		/**
		 * \brief Retourne la valeur de bruit aux coordonnées (x,y)
		 * \param x Coordonnée en x
		 * \param y  Coordonnée en y
		 * \return La valeur de bruit aux coordonnées (x,y)
		 * */
		double getValue(const double x, const double y);

	protected:
		noise::module::Perlin bruit;
		double amplitude;
		double threshold;
		double angle;
		double c_angle;
		double s_angle;
		double shift;
	private:
};

#endif // NOISE_H

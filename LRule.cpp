#include <cassert>

#include <LRule.hpp>

LRule::LRule()
{}

LRule::LRule(const unsigned int nbIter, char symbol, std::string rule) : _symbol(symbol), _rule(rule)
{
	int nb_open = 0;
	for(uint i = 1; i < _rule.size(); ++i)
	{
		if(_rule.at(i) == '[')
		{
			nb_open++;
		}
		else if(_rule.at(i) == ']')
		{
			nb_open--;
		}
		else
		{
			assert(_rule.at(i) == _symbol || _rule.at(i) == '-' || _rule.at(i) == '+'  );
		}
	}
	assert(nb_open == 0);
	_current+=_rule;
	_current = getRule(nbIter);
}


std::string LRule::getRule(const unsigned int nbIter)
{
	for(uint j = 0; j < nbIter; ++j)
	{
		for(uint i = 0; i < _current.size(); ++i)
		{
			if(_current.at(i) == _symbol)
			{
				_current.replace(i, _rule.size(),_rule);
			}
			i+=_rule.size()-1;
		}
	}

	return _current;
}

std::string LRule::getCurrent() const
{
	return _current;
}

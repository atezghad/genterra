#ifndef VEGETATION_H
#define VEGETATION_H

#include <string>

#include <Utils/Disc.hpp>
#include <LRule.hpp>
/**
* \file Environnement.hpp
* \author Anis Tezghadanti
* */
typedef unsigned int uint;
typedef std::vector< std::tuple<Coords2D, double, bool, uint> > offspring;

/**
 * \namespace Environnement
 * \brief Ce namespace contient les classes Espece et Plante qui permettent de définir l'écosystème du Terrain
 * */
namespace Environnement {

	/**
	 * \class Espece
	 * \brief La classe Espece définit une espèce de végétation. Sa règle de production est basée d'après un L-Système partiel
	 * */
	class Espece
	{
		public:

			/**
			 * \brief Constructeur de l'objet Espece
			 * \param newPlants Définit le nombre de nouvelles plantes de l'Espece créés
			 * \param maxSize Définit la taille maximale de l'Espece
			 * \param averageGrowth Définit la vitesse moyenne de croissance
			 * \param probaComp Définit la probabilité de gagner une compétition
			 * \param wetOrDry Définit la préférence de l'Espace envers un climat sec(0.0) ou humide(1.0) ou entre
			 * \param minTemp Définit la température minimale supportée par l'Espece
			 * \param maxTemp Définit la température maximale supportée par l'Espece
			 * \param symbol Définit le symbole servant à définir la règle de production
			 * \param rule Définit la règle de production permettant de représenter la propagation d'une espèce
			 * \param nbIter Dénit le nombre de fois que la règle est produite
			 * \param angle Définit l'angle entre chaque branche
			 * */
			Espece(const uint &newPlants, const double &maxSize,
				   const double &averageGrowth, const double &probaComp,
				   const double &wetOrDry, const double &minTemp,
				   const double &maxTemp,
				   const char symbol, const std::string rule, const uint &nbIter, const double &angle);

			/**
			 * \brief Retourne l'identifiant de l'Espece
			 * \return L'identifiant de l'Espece
			 * */
			uint id() const;

			/**
			 * \brief Retourne l'identifiant de l'Espece
			 * \return L'identifiant de l'Espece
			 * */
			void setId(uint id);

			/**
			 * \brief Retourne le nombre de nouvelles plantes de l'Espece créés
			 * \return Le nombre de nouvelles plantes de l'Espece créés
			 * */
			uint newPlants() const;

			/**
			 * \brief Retourne la taille maximale de l'Espece
			 * \return La taille maximale de l'Espece
			 * */
			double maxSize() const;

			/**
			 * \brief Retourne la vitesse moyenne de croissance
			 * \return La vitesse moyenne de croissance
			 * */
			double averageGrowth() const;

			/**
			 * \brief Retourne la probabilité de gagner une compétition
			 * \return La probabilité de gagner une compétition
			 * */
			double probaComp() const;

			/**
			 * \brief Retourne la préférence de l'Espace envers un climat sec(0.0) ou humide(1.0) ou entre
			 * \return La préférence de l'Espace envers un climat sec(0.0) ou humide(1.0) ou entre
			 * */
			double wetOrDry() const;

			/**
			 * \brief Retourne la température minimale supportée par l'Espece
			 * \return La température minimale supportée par l'Espece
			 * */
			double minTemp() const;

			/**
			 * \brief Retourne la température maximale supportée par l'Espece
			 * \return La température maximale supportée par l'Espece
			 * */
			double maxTemp() const;

			/**
			 * \brief Retourne l'arboresence de propagation définie par la règle de production
			 * \return L'arboresence de propagation définie par la règle de production
			 * */
			std::string arborescence() const;

			/**
			 * \brief Retourne l'angle entre chaque branche
			 * \return L'angle entre chaque branche
			 * */
			double angle() const;

			/**
			 * \brief Retourne le symbole servant à définir la règle de production
			 * \return Le symbole servant à définir la règle de production
			 * */
			char symbol() const;

			/**
			 * \brief La couleur représentant l'Espece
			 * */
			int colors[3];

			/**
			 * \brief Retourne l'écart entre les branches dans l'axe X
			 * \return L'écart entre les branches dans l'axe X
			 * */
			int dx() const;

			/**
			 * \brief Retourne l'écart entre les branches dans l'axe Y
			 * \return L'écart entre les branches dans l'axe Y
			 * */
			int dy() const;


		protected:
			uint _id;
			uint _newPlants;
			double _maxSize;
			double _averageGrowth;
			double _probaComp;
			double _wetOrDry;
			double _minTemp;
			double _maxTemp;
			double _angle;
			char _symbol;
			std::string _arborescence;
			int _dx;
			int _dy;
			LRule _rule;
	};

	/**
	 * \class Plante
	 * \brief La classe Plante définit un type de plante dérivant d'une espèce.
	 * */
	class Plante
	{
		public:
			/**
			 * \brief Constructeur de l'objet Espece
			 * \param species Définit l'Espece à laquelle appartient la Plante
			 * \param vigor Définit la vigueur de la Plante
			 * \param x Définit la position en X de la Plante
			 * \param y Définit la position en Y de la Plante
			 * \param orientation Définit l'angle d'orientation de la Plante
			 * \param root Définit si la Plante est une racine ou non
			 * */
			Plante(Environnement::Espece& species, const double &vigor,
				   const double &x, const double &y, const double &orientation,
				   const bool &root);

			/**
			 * \brief Retourne la compétitivité actuelle de la Plante
			 * \return La compétitivité actuelle de la Plante
			 * */
			double competitivity() const;

			/**
			 * \brief Retourne l'Espèce à laquelle appartient la Plante
			 * \return Un pointeur de l'Espece à laquelle appartient la Plante
			 * */
			Environnement::Espece* species() const;

			/**
			 * \brief Modifie l'Espèce à laquelle appartient la Plante
			 * \param species L'Espece à laquelle appartiendra la Plante
			 * */
			void setSpecies(Environnement::Espece &species);

			/**
			 * \brief Retourne la vigeur de la Plante
			 * \return La vigeur de la Plante
			 * */
			double vigor() const;

			/**
			 * \brief Modifie la vigeur de la Plante
			 * \param La nouvelle valeur de la vigueur
			 * */
			void setVigor(double &vigor);

			/**
			 * \brief Retourne le disque représentant la Plante dans l'espace
			 * \return Le disque représentant la Plante dans l'espace
			 * */
			Disc disc() const;

			/**
			 * \brief Retourne l'égalité entre deux plantes
			 * \param p Une Plante
			 * \return true si les deux Plantes sont au même endroit et appartiennent à la même Espce, false sinon
			 * */
			bool operator ==(const Environnement::Plante &p) const;

			/**
			 * \brief Retourne la différence entre deux plantes
			 * \param p Une Plante
			 * \return false si les deux Plantes sont au même endroit et appartiennent à la même Espce, true sinon
			 * */
			bool operator !=(const Environnement::Plante &p) const;

			/**
			 * \brief Modifie le disque représentant la Plante dans l'espace
			 * \param Le nouveau disque représentant la Plante dans l'espace
			 * */
			void setDisc(const Disc &disc);

			/**
			 * \brief Fait croître la Plante en fonction de ses paramètres
			 * \param humidity L'humidité en un point
			 * \param temperature La température en un point
			 * */
			void grow(const double humidity, const double temperature);

			/**
			 * \brief Indique si il y a compétition entre 2 Plantes (intersection de leurs disques)
			 * \return true si intersection false sinon
			 * */
			bool competition(const Environnement::Plante &p);

			/**
			 * \brief Retourne la propagation de la Plante selon la règle de production de son Espece
			 * \return Les différentes positions des nouvelles plantes
			 * */
			offspring spread();

			/**
			 * \brief Retourne si une Plante est racine
			 * \return true si la Plante est racine false sinon
			 * */
			bool root() const;

			/**
			 * \brief Modifie si une Plante est racine
			 * \param root
			 * */
			void setRoot(const bool &root);

			static double VIGOR_MIN;

		protected:
			Environnement::Espece * _species;
			double _vigor;
			Disc _disc;
			double _competitivity;
			uint _age;
			double _orientation;
			bool _root;
			int _old;

	};

}


#endif // VEGETATION_H

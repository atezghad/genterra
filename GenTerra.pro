#-------------------------------------------------
#
# Project created by QtCreator 2016-10-01T16:02:24
#
#-------------------------------------------------

QT       += core

greaterThan(QT_MAJOR_VERSION, 4):

TARGET = GenTerra
TEMPLATE = app


SOURCES += main.cpp\
    Terrain.cpp \
    Octave.cpp \
    Utils/Disc.cpp \
Environnement.cpp \
    znoise/src/module/modulebase.cpp \
    znoise/src/module/perlin.cpp \
    znoise/src/noisegen.cpp \
    Utils/Coords2D.cpp \
    Utils/Triangle.cpp \
    Utils/Coords3D.cpp \
    Utils/Vec3D.cpp \
    Utils/Vertex.cpp \
    Utils/FaceCirculator.cpp \
    Utils/VertexCirculator.cpp \
    Materiau.cpp \
    LRule.cpp \
    Utils/Random.cpp

HEADERS  += \
    Octave.hpp \
    Terrain.hpp \
    Utils/CImg.h \
    Utils/Disc.hpp \
    Environnement.hpp \
    znoise/src/module/modulebase.h \
    znoise/src/module/perlin.h \
    znoise/src/basictypes.h \
    znoise/src/exception.h \
    znoise/src/noisegen.h \
    znoise/src/interp.h \
    znoise/src/vectortable.h \
    Utils/Coords2D.hpp \
    Utils/Triangle.hpp \
    Utils/Coords3D.hpp \
    Utils/Vec3D.hpp \
    Utils/Vertex.hpp \
    Utils/FaceCirculator.hpp \
    Utils/VertexCirculator.hpp \
    Materiau.hpp \
    LRule.hpp \
    Utils/Random.hpp

LIBS += -lX11 -lpng -lpthread -fopenmp
QMAKE_CXXFLAGS += -fopenmp -std=c++11

DISTFILES += \
    znoise/src/Sources
